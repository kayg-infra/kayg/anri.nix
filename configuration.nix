{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in

{
  imports = [
    # include results of the hardware scan
    ./hardware-configuration.nix
    # include commonly used options
    ./common/system/boot.nix
    ./common/system/common.nix
    ./common/system/docker.nix
    ./common/system/encryption.nix
    ./common/system/environment.nix
    ./common/system/networking.nix
    # include locally used options
    ./networking.nix
    ./services.nix
    ./users.nix
    # include home-manager
    <home-manager/nixos>
  ];

  # START OF --- boot.nix --- ###
  boot.loader.grub = {
    efiSupport = false;
    device = "/dev/nvme0n1";
  };

  boot.initrd.availableKernelModules = [ "e1000e" ];

  boot.initrd.network.postCommands = "
    echo 'cryptsetup-askpass' >> /root/.profile
  ";

  boot.initrd.network.ssh.authorizedKeys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHxMfo794c476zO54g8N/WrhIoniUNxYrysAGP5PfnAa kayg@nana"
  ];

  boot.initrd.luks.devices."root".allowDiscards = true;

  # use hardened kernel
  boot.kernelPackages = pkgs.linuxPackages;
  # END OF --- boot.nix --- ###

  # START OF --- environment.nix --- ###
    environment.systemPackages = with unstable; [
    mergerfs
    rclone
  ];
  # END OF --- environment.nix --- ###

  # START OF --- filesystems.nix --- ###
    fileSystems."/".options = [
    "defaults"
    "commit=60"
    "noatime"
    "nobarrier"
  ];

  fileSystems."/boot".options = [
    "defaults"
    "commit=60"
    "noatime"
    "nobarrier"
  ];
  # END OF --- filesystems.nix --- #

  # START OF --- networking.nix --- #
  networking.hostName = "anri";
  networking.firewall.enable = true;

  /* ipv4 section */
  networking.interfaces.eth0.ipv4.addresses = [ {
      address = "46.4.107.150";
      prefixLength = 19;
    }
  ];

  networking.defaultGateway = {
    address = "46.4.107.129";
    interface = "eth0";
  };

  /* ipv6 section */
  networking.interfaces.eth0.ipv6.addresses = [ {
      address = "2a01:4f8:141:41b::1";
      prefixLength = 64;
    }
  ];

  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "eth0";
  };

  /* Linux has something called AnyIP which allows a subnet to be bound to an
  interface instead of a single IP address. This is particularly useful for
  ipv6, in which case providers assign /64 blocks a single server, allowing the
  user to choose between 16^16 addresses. It would become rather cumbersome to
  assign addresses one by one.

  The loopback interface already makes use of Linux's AnyIP.
  For example: ip addr show lo
  will output this: inet 127.0.0.1/8 scope host lo
  which basically says that all IPs from 127.0.0.1 to 127.0.0.255 belong to that host.

  Similarly, adding a subnet as a route to the loopback interface will bind the
  whole subnet to the host.

  The implementation here: https://github.com/NixOS/nixpkgs/blob/release-20.03/nixos/modules/tasks/network-interfaces-scripted.nix#L214
  doesn't allow for the following to work as there's no ${var} between `ip route add` and ${cidr}.

  networking.interfaces.lo.ipv6.routes = [ {
      address = "2a02:c207:2038:2340::1";
      prefixLength = 64;
      options = {
        to = "local";
      };
    }
  ];

  For now, this will have to do:
  networking.localCommands = ''
    ip route add local 2a02:c207:2038:2340::1/64 dev lo
  ''; */
  # END OF --- networking.nix --- #

  # START OF --- services.nix --- #
   /* Borg backup jobs */
  services.borgbackup.jobs = {
    kayg = {
      compression = "zstd";
      /* Repository is on an external mount */
      doInit = false;
      encryption = {
        mode = "keyfile-blake2";
        passCommand = "cat /root/.vault/borg/passphrase";
      };
      environment = {
        BORG_RSH = "ssh -p 897 -i /root/.vault/borg/ssh";
        BORG_KEY_FILE = "/root/.vault/borg/keyfile";
      };
      group = "users";
      paths = "/stacks";
      repo = "borg@backups.kayg.org:/backups/kayg/anri";
      startAt = "05:00";
      user = "root";
    };
  };

  /* Borg backup repositories */
  /* Making multiple repositories poses a problem with the rclone mount because:
  1. rclone runs as an unprivileged user
  2. borg runs as root

  So when the rclone mount breaks for some reason or even in normal cases with a
  `nixos-rebuild switch`; for the brief amount of time between the unmount and
  the remount, borg sees that the repo directories inside /backups do not exist
  and hence proceeds to create them. Since borg runs as root, it has no issues
  with overriding permissions.

  I figured I might as well go with one fat borg repo with subrepos, which
  allows flexibility, instead of different repos with subrepos. */
  services.borgbackup.repos.backups = {
    allowSubRepos = true;
    authorizedKeys = [
      /* kayg */
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINk27uX6mEPiQfIpg32wIalJLM63mEMqFef7YHuYvUvy root@anri"
      /* sasach */
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKtM+z2/2HSD+Wqa0BVgR7E5T08X7AhvinCDKVmTMYua sasach@work"
      /* pandacowbat */
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAQn2NzWn/ldJU91rNdy3TSisPOhkZapyPRx06lw+ZR9 root@vmi281403.contaboserver.net"
    ];
    /* all files in this directory must be owned by the user 'borg' */
    path = /backups;
  };

  /* Mount drive for backups */
  systemd.services.rclone-mount-backups = {
    after = [
      "network-online.target"
    ];
    description = "Mount encrypted drive at /backups";
    environment = {
      RCLONE_CONFIG= "/home/kayg/.config/rclone/rclone.conf";
    };
    path = [
      "/run/wrappers"
      unstable.rclone
    ];
    restartIfChanged = true;
    serviceConfig = {
      ExecStart = ''
     ${unstable.rclone}/bin/rclone mount backup-drive-1-encrypted:/backups /backups \
       --allow-other \
       --buffer-size 256M \
       --dir-cache-time 1000h \
       --umask 002 \
       --vfs-cache-mode writes
    '';
      ExecStop = ''
     /run/wrappers/bin/fusermount -uz /backups
    '';
      Group = "users";
      KillMode = "none";
      Restart = "on-failure";
      RestartSec = 5;
      Type = "notify";
      User = "kayg";
    };
    wantedBy = [
      "multi-user.target"
    ];
  };

  services.tailscale.enable = true;
  ### END OF --- services.nix ###

  users = {
    users = {
      # Define user accounts
      kayg = {
        createHome = true;
        extraGroups = [ "wheel" "docker" ]; # Enable ‘sudo’ for the user.
        hashedPassword = "$6$VNu7UUuVDBhWk$PQ8X0kjFnDnpUs59oNpnx3Vb3IwYPsU9JhyZoQLL9BVItc4BLngE2gLvNWlsjvu9/jGNgnwDZIEpdik58K/0K0";
        home = "/home/kayg";
        isNormalUser = true;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEe1RQI434JBwvurjH3wkpL9VKAOUnNt3lRUMl+Nrom3 kayg@nana"
        ];
        shell = pkgs.zsh;
        uid = 1000;
      };
    };
  };

  # newer contabo VPS contain AMD processors
  hardware.cpu.intel.updateMicrocode = true;

  home-manager.users.kayg = import ./common/home/home.nix;

  programs.fuse.userAllowOther = true;

  time.timeZone = "Europe/Berlin";
}
